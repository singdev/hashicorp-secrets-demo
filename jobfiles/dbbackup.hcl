job "database-backup" {
    region = "${region}"
    datacenters = ["${datacenter}"]
    type = "batch"
    periodic {
        # run every 12 hours
        cron             = "0 */12 * * * *"
    }
    group "dbbackup-group" {
        task "dbbackup_task" {
            driver = "docker"
            vault {
                # here we are granting this job access to the vault policy we created earlier 
                policies = ["aws-s3-dbbackup-policy"]
            }
            template {
                data = <<DATA
# pull AWS credentials from the designated aws role.
# granting the policy above allows us to read from this secrets path
{{with secret "aws/creds/aws-s3-dbbackup"}}
AWS_ACCESS_KEY_ID={{.Data.access_key}}
AWS_SECRET_ACCESS_KEY={{.Data.secret_key}}
{{end}}
AWS_DEFAULT_REGION=us-east-1
DATA
                # store env file in the /secrets directory so secrets are kept in memory, not on disk.
		  		destination = "secrets/file.env"
		  		env         = true
            }
            resources {
                cpu    = 600
                memory = 800
                network {
                    mbits = 1
                }
            }
            config {
                image = "${dbbackup_image}"
            }
        }
    }  
}