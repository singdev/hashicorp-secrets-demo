provider "vault" {
  address = "${var.vault_endpoint}"
  token = "${var.vault_token}"
  # will likely need some SSL information here too.
} 