data "template_file" "dbbackup-jobspec" {
  template = "${file("jobfiles/dbbackup.hcl")}"
  vars {
    datacenter      = "${var.consul_datacenter}"
    region          = "${var.consul_region}"
    dbbackup_image  = "${var.dbbackup_image}"
  }
}

resource "nomad_job" "job" {
    depends_on = ["vault_policy.dbbackup-policy"]
    #render the jobspec nomad document
    jobspec               = "${data.template_file.dbbackup-jobspec.rendered}"
    deregister_on_destroy = true
} 