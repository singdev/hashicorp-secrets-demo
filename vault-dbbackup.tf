data "template_file" "dbbackup_role_policy_json" {
    template = "${file("policies/s3_single_folder_full_access.json")}"
    vars {
        backup_bucket = "${var.backup_bucket}"
        backup_path = "${var.backup_path}"
    }
}

resource "vault_aws_secret_backend_role" "role" {
    backend = "aws"
    name    = "aws-s3-dbbackup"
    # here we pass in the rendered policy template
    policy = "${data.template_file.dbbackup_role_policy_json.rendered}"
}

resource "vault_policy" "dbbackup-policy" {
    # this is the name we must specify in the vault stanza of the nomad job.
    name = "aws-s3-dbbackup-policy"
    # inline vault policy!
    policy = <<POLICY
path "aws/creds/aws-s3-dbbackup" {
  capabilities = ["read"]
}
POLICY
}