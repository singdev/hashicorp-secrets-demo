# Configure the Nomad provider
provider "nomad" {
  address = "${var.nomad_endpoint}"
  region  = "${var.region}"
}