variable "consul_region" {
    default = "north-america"
}
variable "consul_datacenter" {
    default = "dc"
}

variable "nomad_endpoint" {
    default = "http://nomad.service.consul:4646"
}

variable "vault_endpoint" {
    default = "https://vault.service.consul:8200"
}
variable "vault_token" {}

variable "dbbackup_image" {}

variable "backup_bucket" {
    default = "my_bucket"
}
variable "backup_path" {
    default = "dbbackup/"
}